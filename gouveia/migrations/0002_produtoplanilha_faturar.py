# -*- coding: utf-8 -*-

# Generated by Django 1.10 on 2016-12-19 09:07


from django.db import migrations
import djangoplus.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('gouveia', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='produtoplanilha',
            name='faturar',
            field=djangoplus.db.models.fields.BooleanField(default=False, help_text='Marque essa op\xe7\xe3o caso deseje que este item seja adicionado \xe0 nota fiscal.', verbose_name='Faturar?'),
        ),
    ]
