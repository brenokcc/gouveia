# Generated by Django 2.1.7 on 2019-10-07 12:57

from django.db import migrations
import djangoplus.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('gouveia', '0019_auto_20190414_1522'),
    ]

    operations = [
        migrations.AddField(
            model_name='revisaoveicular',
            name='observacao',
            field=djangoplus.db.models.fields.TextField(blank=True, null=True, verbose_name='Observação'),
        ),
    ]
