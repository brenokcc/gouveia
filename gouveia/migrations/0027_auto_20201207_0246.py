# Generated by Django 2.1.7 on 2020-12-07 02:46

from django.db import migrations
import djangoplus.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('gouveia', '0026_remove_proposta_projetos'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='equipamentoprotecao',
            options={},
        ),
        migrations.RemoveField(
            model_name='equipamentoprotecao',
            name='data_entrega',
        ),
        migrations.RemoveField(
            model_name='equipamentoprotecao',
            name='funcionario',
        ),
        migrations.RemoveField(
            model_name='equipamentoprotecao',
            name='quantidade',
        ),
        migrations.AlterField(
            model_name='equipamentoprotecao',
            name='data_compra',
            field=djangoplus.db.models.fields.DateField(blank=True, null=True, verbose_name='Data da Compra'),
        ),
        migrations.AlterField(
            model_name='equipamentoprotecao',
            name='data_validade',
            field=djangoplus.db.models.fields.DateField(blank=True, null=True, verbose_name='Data de Validade'),
        ),
        migrations.AlterField(
            model_name='equipamentoprotecao',
            name='numero_ca',
            field=djangoplus.db.models.fields.CharField(blank=True, max_length=255, null=True, verbose_name='Nº do CA'),
        ),
    ]
