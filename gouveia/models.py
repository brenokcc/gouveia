# -*- coding: utf-8 -*-
import xmltodict
import os
from django.conf import settings
from datetime import datetime
from datetime import date
from django.core.exceptions import ValidationError
from django.template.loader import render_to_string
from djangoplus.db import models
from djangoplus.decorators import meta, action, subset, role
from django.db.models.aggregates import Sum
from django.db.models.expressions import F
from decimal import Decimal
from gouveia.nfe import NfeAPI
from djangoplus.utils.dateutils import add_days, MONTH_NAMES, parse_date


class Pais(models.Model):

    nome = models.CharField('Nome', search=True)
    codigo = models.CharField('Código', search=True)

    fieldsets = (
        ('Dados Gerais', {'fields': ('nome', 'codigo')}),
    )

    class Meta:
        verbose_name = 'País'
        verbose_name_plural = 'Paises'
        menu = 'Cadastros Gerais::Localidades::Paises', 'fa-th'

    def __str__(self):
        return '%s' % self.nome


class Estado(models.Model):

    nome = models.CharField('Nome', search=True)
    sigla = models.CharField('Sigla', search=True)
    codigo = models.CharField('Código', search=True)
    pais = models.ForeignKey(Pais, verbose_name='País', null=True, blank=False, default=1)

    fieldsets = (
        ('Dados Gerais', {'fields': ('nome', 'sigla', 'codigo', 'pais')}),
    )

    class Meta:
        verbose_name = 'Estado'
        verbose_name_plural = 'Estados'
        menu = 'Cadastros Gerais::Localidades::Estados', 'fa-th'

    def __str__(self):
        return '%s' % self.sigla


class Municipio(models.Model):
    estado = models.ForeignKey(Estado, verbose_name='Estado')
    nome = models.CharField(verbose_name='Nome', search=True)
    codigo = models.CharField('Código', search=True)

    fieldsets = (
        ('Dados Gerais', {'fields': ('estado', 'nome', 'codigo')}),
    )

    class Meta:
        verbose_name = 'Município'
        verbose_name_plural = 'Municípios'
        menu = 'Cadastros Gerais::Localidades::Municípios', 'fa-th'
        list_per_page = 100

    def __str__(self):
        return '%s/%s' % (self.nome, self.estado)


@role('cpf')
class Administrador(models.Model):
    cpf = models.CpfField(verbose_name='CPF', search=True)
    nome = models.CharField('Nome', search=True)
    email = models.CharField('E-mail', blank=True, default='')
    telefone_principal = models.PhoneField('Telefone Principal', null=True, blank=True)
    telefone_secundario = models.PhoneField('Telefone Secundário', null=True, blank=True)

    fieldsets = (
        ('Dados Gerais', {'fields': (('cpf', 'nome'), 'email')}),
        ('Dados para Contato', {'fields': (('telefone_principal', 'telefone_secundario'),)}),
    )

    class Meta:
        verbose_name = 'Administrador'
        verbose_name_plural = 'Administradores'
        menu = 'Usuários::Administradores', 'fa-users'
        icon = 'fa-users'
        can_admin = 'Administrador'

    def __str__(self):
        return '%s' % self.nome


@role('cpf')
class GerenteEstoque(models.Model):
    cpf = models.CpfField(verbose_name='CPF', search=True)
    nome = models.CharField('Nome', search=True)
    email = models.CharField('E-mail', blank=True, default='')
    telefone_principal = models.PhoneField('Telefone Principal', null=True, blank=True)
    telefone_secundario = models.PhoneField('Telefone Secundário', null=True, blank=True)

    fieldsets = (
        ('Dados Gerais', {'fields': (('cpf', 'nome'), 'email')}),
        ('Dados para Contato', {'fields': (('telefone_principal', 'telefone_secundario'),)}),
    )

    class Meta:
        verbose_name = 'Gerente de Estoque'
        verbose_name_plural = 'Gererentes de Estoque'
        menu = 'Usuários::Gererentes de Estoque', 'fa-users'
        icon = 'fa-users'
        can_admin = 'Administrador'

    def __str__(self):
        return '%s' % self.nome


@role('cpf')
class GerenteFinanceiro(models.Model):
    cpf = models.CpfField(verbose_name='CPF', search=True)
    nome = models.CharField('Nome', search=True)
    email = models.CharField('E-mail', blank=True, default='')
    telefone_principal = models.PhoneField('Telefone Principal', null=True, blank=True)
    telefone_secundario = models.PhoneField('Telefone Secundário', null=True, blank=True)

    fieldsets = (
        ('Dados Gerais', {'fields': (('cpf', 'nome'), 'email')}),
        ('Dados para Contato', {'fields': (('telefone_principal', 'telefone_secundario'),)}),
    )

    class Meta:
        verbose_name = 'Gerente Financeiro'
        verbose_name_plural = 'Gererentes Financeiros'
        menu = 'Usuários::Gererentes Financeiros', 'fa-users'
        icon = 'fa-users'
        can_admin = 'Administrador'

    def __str__(self):
        return '%s' % self.nome


@role('cpf')
class Engenheiro(models.Model):
    cpf = models.CpfField(verbose_name='CPF', search=True)
    nome = models.CharField('Nome', search=True)
    email = models.CharField('E-mail', blank=True, default='')
    telefone_principal = models.PhoneField('Telefone Principal', null=True, blank=True)
    telefone_secundario = models.PhoneField('Telefone Secundário', null=True, blank=True)

    fieldsets = (
        ('Dados Gerais', {'fields': (('cpf', 'nome'), 'email')}),
        ('Dados para Contato', {'fields': (('telefone_principal', 'telefone_secundario'),)}),
    )

    class Meta:
        verbose_name = 'Engenheiro'
        verbose_name_plural = 'Engenheiros'
        menu = 'Usuários::Engenheiros', 'fa-users'
        icon = 'fa-users'
        can_admin = 'Administrador'

    def __str__(self):
        return '%s' % self.nome


class Pessoa(models.Model):
    nome = models.CharField('Nome', null=False, blank=False, search=True)
    documento = models.CharField('CPF/CNPJ', null=False, blank=False, search=True, exclude=True)

    class Meta:
        verbose_name = 'Pessoa'
        verbose_name_plural = 'Pessoa'

    def __str__(self):
        return self.nome


class PessoaJuridica(Pessoa):
    cnpj = models.CnpjField(verbose_name='CNPJ', null=True, blank=True, search=True)
    ie = models.CharField(verbose_name='Inscrição Estadual', null=True, blank=True)
    nome_fantasia = models.CharField(verbose_name='Nome Fantasia', search=True, blank=True, default='')
    telefone_principal = models.PhoneField('Telefone Principal', null=True, blank=True)
    telefone_secundario = models.PhoneField('Telefone Secundário', null=True, blank=True)
    responsavel = models.CharField('Contato', help_text='Nome do funcionário com que os contatos são estabelecidos', null=True, blank=True)

    logradouro = models.CharField(verbose_name='Rua/Avenida', null=True, blank=True)
    numero = models.CharField(verbose_name='Número', null=True, blank=True)
    complemento = models.CharField(verbose_name='Complemento', null=True, blank=True)
    bairro = models.CharField(verbose_name='Bairro', null=True, blank=True)
    cep = models.CepField(verbose_name='CEP', null=True, blank=True)
    municipio = models.ForeignKey(Municipio, verbose_name='Cidade', null=True, blank=True, lazy=True)

    class Meta:
        verbose_name = 'Pessoa Jurídica'
        verbose_name_plural = 'Pessoas Jurídicas'
        menu = 'Pessoal::Pessoas Jurídicas', 'fa-user'
        icon = 'fa-building'
        can_admin = 'Gerente Financeiro', 'Gerente de Estoque', 'Engenheiro'
        add_shortcut = True
        verbose_female = True
        list_display = 'nome', 'nome_fantasia', 'cnpj'

    fieldsets = (
        ('Dados Gerais', {'fields': ('nome', 'nome_fantasia', ('cnpj', 'ie'),)}),
        ('Dados para Contato', {'fields': (('telefone_principal', 'telefone_secundario'), 'responsavel')}),
        ('Endereço', {'fields': (('logradouro', 'numero'), ('complemento', 'bairro'), ('cep', 'municipio'))}),
    )

    def __str__(self):
        return '%s (%s)' % (self.nome, self.cnpj)

    def save(self, *args, **kwargs):
        self.documento = self.cnpj
        super(PessoaJuridica, self).save(*args, **kwargs)

    @action('Consultar Cadastro (SEFAZ)', condition='cnpj', inline=True)
    def consultar_cadastro(self):
        api = NfeAPI('09432a22249879493e6becb77d4b767c13f5635c')
        dados = api.cadastro(self.cnpj)
        if dados:
            self.municipio = Municipio.objects.get(codigo=dados['municipio'])
            self.nome = dados['nome']
            self.nome_fantasia = dados['nome_fantasia']
            self.logradouro = dados['logradouro']
            self.numero = dados['numero']
            self.bairro = dados['bairro']
            self.ie = dados['ie']
            self.municipio.codigo = dados['municipio']
            self.cep = dados['cep']
            self.save()
        else:
            raise ValidationError('Cadastro não consta na base de dados da SEFAZ.')

    @staticmethod
    def formatar_cnpj(n):
        return '{}.{}.{}/{}-{}'.format(n[0:2], n[2:5], n[5:8], n[8:12], n[12:])


class PessoaFisica(Pessoa):
    cpf = models.CpfField(verbose_name='CPF', null=True, blank=True, search=True)
    telefone_principal = models.PhoneField('Telefone Principal', null=True, blank=True)
    telefone_secundario = models.PhoneField('Telefone Secundário', null=True, blank=True)

    logradouro = models.CharField(verbose_name='Rua/Avenida', null=True, blank=True)
    numero = models.CharField(verbose_name='Número', null=True, blank=True)
    complemento = models.CharField(verbose_name='Complemento', null=True, blank=True)
    bairro = models.CharField(verbose_name='Bairro', null=True, blank=True)
    cep = models.CepField(verbose_name='CEP', null=True, blank=True)
    municipio = models.ForeignKey(Municipio, verbose_name='Cidade', null=True, blank=True, lazy=True)

    class Meta:
        verbose_name = 'Pessoa Física'
        verbose_name_plural = 'Pessoas Físicas'
        menu = 'Pessoal::Pessoas Físicas', 'fa-user'
        icon = 'fa-user'
        can_admin = 'Gerente Financeiro'
        add_shortcut = True
        verbose_female = True
        list_display = 'nome', 'cpf'

    fieldsets = (
        ('Dados Gerais', {'fields': (('nome', 'cpf'),)}),
        ('Dados para Contato', {'fields': (('telefone_principal', 'telefone_secundario'))}),
        ('Endereço', {'fields': (('logradouro', 'numero'), ('complemento', 'bairro'), ('cep', 'municipio'))}),
    )

    def save(self, *args, **kwargs):
        self.documento = self.cpf
        super(PessoaFisica, self).save(*args, **kwargs)

    def __str__(self):
        return '%s (%s)' % (self.nome, self.cpf)


class UnidadeMedida(models.Model):
      
    descricao = models.CharField('Descrição', null=False, blank=False, search=True)
    
    fieldsets = (
        ('Dados Gerais', {'fields': ('descricao',),}),
    )
    
    class Meta:
        verbose_name = 'Unidade de Medida'
        verbose_name_plural = 'Unidades de Medida'
        icon = 'fa-balance-scale'
        can_admin = 'Engenheiro'
        menu = 'Almoxarifado::Cadastros::Unidades de Medida', 'fa-crop'
    
    def __str__(self):
        return self.descricao


class NCM(models.Model):
    descricao = models.CharField(verbose_name='Descrição', search=True)
    codigo = models.CharField(verbose_name='Código', search=True)

    class Meta:
        verbose_name = 'NCM'
        verbose_name_plural = 'NCM'
        verbose_female = False
        menu = 'Fiscal::Cadastros::NCM', 'fa-paw'
        list_per_page = 200
        can_list = 'Gerente Financeiro'

    def __str__(self):
        return '%s - %s' % (self.codigo, self.descricao)


class CFOP(models.Model):
    descricao = models.CharField(verbose_name='Descrição', search=True)
    codigo = models.CharField(verbose_name='Código', search=True)

    class Meta:
        verbose_name = 'CFOP'
        verbose_name_plural = 'CFOP'
        verbose_female = False
        menu = 'Fiscal::Cadastros::CFOP', 'fa-paw'
        list_per_page = 200
        can_list = 'Gerente Financeiro'

    def __str__(self):
        return '%s - %s' % (self.codigo, self.descricao)


class Produto(models.Model):
    descricao = models.CharField('Descrição', null=False, blank=False, search=True)
    unidade_medida = models.ForeignKey(UnidadeMedida, verbose_name='Unidade de Medida', filter=True)
    valor_unitario = models.DecimalField('Valor Unitário')
    ncm = models.ForeignKey(NCM, verbose_name='NCM', lazy=True, null=True, blank=True)
    cfop = models.ForeignKey(CFOP, verbose_name='CFOP', lazy=True, null=True, blank=True)
    quantidade_inicial = models.DecimalField('Quantidade Inicial', exclude=True, default=0)

    fieldsets = (
        ('Dados Gerais', {'fields': ('descricao', ('unidade_medida', 'valor_unitario'), ('ncm', 'cfop'))}),
        ('Estoque::Resumo', {'fields': ('quantidade_inicial', ('get_qtd_entrada', 'get_qtd_saida'), ('get_qtd_estoque', 'get_valor_estoque')), 'actions': ('informar_estoque_atual', )}),
        ('Notas Fiscais::Notas Fiscais', {'relations': ('get_notas_fiscais',)}),
        ('Orçamentos::Orçamentos', {'relations': ('get_orcamentos',)}),
    )

    class Meta:
        verbose_name = 'Produto'
        verbose_name_plural = 'Produtos'
        icon = 'fa-th-list'
        can_admin = 'Engenheiro', 'Gerente de Estoque', 'Gerente Financeiro'
        menu = 'Almoxarifado::Produtos', 'fa-crop'
        list_display = 'descricao', 'unidade_medida', 'valor_unitario', 'ncm', 'cfop', 'get_qtd_estoque', 'get_valor_estoque'
        list_shortcut = True

    def __str__(self):
        return self.descricao

    @meta('Qtd Entrada')
    def get_qtd_entrada(self):
        return self.get_itens_nota_fiscal().filter(nota_fiscal__tipo='0').aggregate(Sum('quantidade')).get('quantidade__sum') or Decimal(0)

    @meta('Qtd Saída')
    def get_qtd_saida(self):
        saida = self.get_itens_nota_fiscal().filter(nota_fiscal__tipo='1').aggregate(Sum('quantidade')).get('quantidade__sum') or Decimal(0)
        saida += ProdutoPlanilha.objects.filter(produto=self, planilha__material_separado=True).aggregate(Sum('quantidade')).get('quantidade__sum') or Decimal(0)
        saida += EstruturaPlanilha.objects.filter(estrutura__itemestrutura__produto=self, planilha__material_separado=True).aggregate(total=Sum(F('quantidade') * F('estrutura__itemestrutura__quantidade'), output_field=models.DecimalField())).get('total') or Decimal(0)
        return saida

    @meta('Qtd em Estoque')
    def get_qtd_estoque(self):
        entrada = self.get_qtd_entrada()
        saida = self.get_qtd_saida()
        return self.quantidade_inicial + entrada - saida

    @meta('Valor em Estoque (R$)')
    def get_valor_estoque(self):
        return self.get_qtd_estoque() * self.valor_unitario

    def get_itens_nota_fiscal(self):
        return ItemNotaFiscal.objects.filter(produto__produto=self)

    @meta('Notas Fiscais')
    def get_notas_fiscais(self):
        return NotaFiscal.objects.filter(id__in=self.get_itens_nota_fiscal().values_list('pk', flat=True))

    @meta('Orçamentos')
    def get_orcamentos(self):
        ids = []
        ids.extend(ProdutoPlanilha.objects.filter(produto=self).values_list('planilha', flat=True))
        ids.extend(EstruturaPlanilha.objects.filter(estrutura__itemestrutura__produto=self).values_list('planilha', flat=True))
        return Planilha.objects.filter(id__in=set(ids))

    def informar_estoque_atual_initial(self):
        self.quantidade_inicial = 0
        return dict(quantidade_inicial=self.quantidade_inicial)

    @action('Informar Estoque Atual')
    def informar_estoque_atual(self, quantidade_inicial):
        self.quantidade_inicial = self.get_qtd_saida() - self.get_qtd_entrada()
        self.quantidade_inicial += quantidade_inicial
        self.save()


class ProdutoFiscalManager(models.DefaultManager):
    @subset('Não-Vinculados ao Almoxarifado')
    def nao_vinculados_almoxarifado(self):
        return self.filter(produto__isnull=True)


class ProdutoFiscal(models.Model):
    descricao = models.CharField('Descrição', null=False, blank=False, search=True)
    ncm = models.ForeignKey(NCM, verbose_name='NCM', lazy=True, null=True, blank=True)
    cfop = models.ForeignKey(CFOP, verbose_name='CFOP', lazy=True, null=True, blank=True)
    unidade_medida = models.ForeignKey(UnidadeMedida, verbose_name='Unidade de Medida', filter=True)
    valor_unitario = models.DecimalField('Valor Unitário')
    produto = models.ForeignKey(Produto, verbose_name='Produto', null=True, blank=True)

    fieldsets = (
        ('Dados Gerais', {'fields': ('descricao', ('ncm', 'cfop'), ('unidade_medida', 'valor_unitario')),}),
        ('Almoxarifado', {'fields': ('produto',), 'actions': ('alterar_vinculo_ao_almoxarifado',)}),
        ('Itens de Nota Fiscal', {'relations': ('get_itens_nota_fiscal',)})
    )

    class Meta:
        verbose_name = 'Produto'
        verbose_name_plural = 'Produtos'
        icon = 'fa-th-list'
        can_admin = 'Engenheiro', 'Gerente de Estoque', 'Gerente Financeiro'
        menu = 'Fiscal::Produtos'
        list_display = 'descricao', 'unidade_medida', 'valor_unitario'

    def __str__(self):
        return self.descricao

    @action('Vincular ao Almoxarifado', subset='nao_vinculados_almoxarifado', condition='not produto')
    def vincular_ao_almoxarifado(self, produto):
        self.produto = produto
        if produto.ncm is None or produto.cfop is None or self.valor_unitario > produto.valor_unitario:
            if produto.ncm is None:
                produto.ncm = self.ncm
            if produto.cfop is None:
                produto.cfop = self.cfop
            if self.valor_unitario > produto.valor_unitario:
                produto.valor_unitario = self.valor_unitario
            produto.save()
        self.save()

    @action('Alterar Vículo ao Almoxarifado', condition='produto')
    def alterar_vinculo_ao_almoxarifado(self, produto):
        self.produto = produto
        self.save()

    @meta('Itens de Nota Fiscal')
    def get_itens_nota_fiscal(self):
        return self.itemnotafiscal_set.all()


class NotaFiscalManager(models.DefaultManager):

    @classmethod
    @action('Importar XML', input='ImportarNotaFiscal')
    def importar(cls, arquivo_xml, gerar_contas=False):
        doc = xmltodict.parse(arquivo_xml)
        chave = doc["nfeProc"]["NFe"]["infNFe"]['@Id'][3:]
        file_path = os.path.join(settings.MEDIA_ROOT, 'nfes', '{}.xml'.format(chave))
        if not os.path.exists(file_path):
            with open(file_path, 'wb') as file:
                arquivo_xml.seek(0)
                file.write(arquivo_xml.read())
        estato = Estado.objects.get(codigo=doc["nfeProc"]["NFe"]["infNFe"]["ide"]["cUF"])
        if "dEmi" in doc["nfeProc"]["NFe"]["infNFe"]["ide"]:
            data_emissao = datetime.strptime(doc["nfeProc"]["NFe"]["infNFe"]["ide"]["dEmi"], "%Y-%m-%d")
        else:
            data_emissao = datetime.strptime(doc["nfeProc"]["NFe"]["infNFe"]["ide"]["dhEmi"][:10], "%Y-%m-%d")
        cnpj_emissor = PessoaJuridica.formatar_cnpj(doc["nfeProc"]["NFe"]["infNFe"]["emit"]["CNPJ"])
        nome_emissor = doc["nfeProc"]["NFe"]["infNFe"]["emit"]["xNome"]
        cnpj_destinatrio = PessoaJuridica.formatar_cnpj(doc["nfeProc"]["NFe"]["infNFe"]["dest"]["CNPJ"])
        nome_destinatrio = doc["nfeProc"]["NFe"]["infNFe"]["dest"]["xNome"]
        serie = doc["nfeProc"]["NFe"]["infNFe"]["ide"]["serie"]

        nota_fiscal = NotaFiscal.objects.filter(chave=chave).first() or NotaFiscal()
        nota_fiscal.chave = chave
        nota_fiscal.estado = estato
        nota_fiscal.serie = serie
        nota_fiscal.data_emissao = data_emissao
        nota_fiscal.emissor = PessoaJuridica.objects.filter(cnpj=cnpj_emissor).first() or PessoaJuridica.objects.create(cnpj=cnpj_emissor, nome=nome_emissor)
        nota_fiscal.destinatario = PessoaJuridica.objects.filter(cnpj=cnpj_destinatrio).first() or PessoaJuridica.objects.create(cnpj=cnpj_destinatrio, nome=nome_destinatrio)
        nota_fiscal.valor = Decimal(doc["nfeProc"]["NFe"]["infNFe"]["total"]["ICMSTot"]["vNF"])
        nota_fiscal.numero = doc["nfeProc"]["NFe"]["infNFe"]['ide']['nNF']
        nota_fiscal.tipo = doc["nfeProc"]["NFe"]["infNFe"]['ide']['tpNF']
        nota_fiscal.save()

        produtos = []
        if type(doc["nfeProc"]["NFe"]["infNFe"]["det"]) == list:
            for item in doc["nfeProc"]["NFe"]["infNFe"]["det"]:
                produtos.append(item['prod'])
        else:
            produtos.append(doc["nfeProc"]["NFe"]["infNFe"]["det"]["prod"])

        for prod in produtos:
            descricao, und, valor_und, valor, desconto, qtd, codigo_ncm, codigo_cfop = prod['xProd'], prod['uCom'], prod['vUnCom'], prod['vProd'], prod.get('vDesc', 0), prod['qCom'], prod['NCM'], prod['CFOP']
            produto = ProdutoFiscal.objects.filter(descricao=descricao).first() or ProdutoFiscal()
            produto.descricao = descricao
            produto.unidade_medida = UnidadeMedida.objects.get_or_create(descricao=und)[0]
            produto.valor_unitario = Decimal(valor_und)
            produto.ncm = NCM.objects.filter(codigo=codigo_ncm).first()
            produto.cfop = CFOP.objects.filter(codigo=codigo_cfop).first()
            produto.save()
            item_nota_fiscal = ItemNotaFiscal.objects.filter(nota_fiscal=nota_fiscal, produto=produto).first() or ItemNotaFiscal()
            item_nota_fiscal.nota_fiscal = nota_fiscal
            item_nota_fiscal.produto = produto
            item_nota_fiscal.valor_unitario = produto.valor_unitario
            item_nota_fiscal.quantidade = Decimal(qtd)
            item_nota_fiscal.valor_desconto = Decimal(desconto)
            item_nota_fiscal.valor_total = Decimal(valor)
            item_nota_fiscal.valor_final = item_nota_fiscal.valor_total - item_nota_fiscal.valor_desconto
            item_nota_fiscal.save()

        if gerar_contas and 'cobr' in doc["nfeProc"]["NFe"]["infNFe"]:
            duplicatas = []
            if 'dup' in doc["nfeProc"]["NFe"]["infNFe"]["cobr"]:
                if type(doc["nfeProc"]["NFe"]["infNFe"]["cobr"]['dup']) == list:
                    for duplicata in doc["nfeProc"]["NFe"]["infNFe"]["cobr"]['dup']:
                        duplicatas.append(duplicata)
                else:
                    duplicatas.append(doc["nfeProc"]["NFe"]["infNFe"]["cobr"]['dup'])
            for duplicata in duplicatas:
                    descricao = 'Duplicata {} da nota fiscal nº {} - {}'.format(duplicata['nDup'], nota_fiscal.numero, nota_fiscal.emissor)
                    conta = ContaPagar.objects.filter(nota_fiscal=nota_fiscal, descricao=descricao).first() or ContaPagar()
                    conta.descricao = descricao
                    conta.nota_fiscal = nota_fiscal
                    conta.data_prevista_pagamento = datetime.strptime(duplicata['dVenc'], "%Y-%m-%d")
                    conta.valor = Decimal(duplicata['vDup'])
                    conta.pessoa = nota_fiscal.emissor
                    conta.tipo_despesa = TipoDespesa.objects.get(descricao='Aquisição de Mercadoria')
                    conta.save()

            if 'fat' in doc["nfeProc"]["NFe"]["infNFe"]["cobr"] and not duplicatas:
                fatura = doc["nfeProc"]["NFe"]["infNFe"]["cobr"]['fat']
                descricao = 'Fatura {} da nota fiscal nº {} - {}'.format(fatura['nFat'], nota_fiscal.numero, nota_fiscal.emissor)
                conta = ContaPagar.objects.filter(nota_fiscal=nota_fiscal, descricao=descricao).first() or ContaPagar()
                conta.descricao = descricao
                conta.nota_fiscal = nota_fiscal
                conta.data_prevista_pagamento = data_emissao
                conta.valor = Decimal(fatura['vLiq'])
                conta.pessoa = nota_fiscal.emissor
                conta.tipo_despesa = TipoDespesa.objects.get(descricao='Aquisição de Mercadoria')
                conta.save()


class NotaFiscal(models.Model):
    numero = models.CharField(verbose_name='Número', search=True)
    tipo = models.CharField(verbose_name='Tipo', filter=True, choices=[['0', 'Entrada'], ['1', 'Saída']])
    serie = models.IntegerField(verbose_name='Série', null=True)
    chave = models.CharField(verbose_name='Chave', search=True)
    estado = models.ForeignKey(Estado, verbose_name='Estado')
    data_emissao = models.DateField(verbose_name='Data da Emissão', filter=True)
    emissor = models.ForeignKey(Pessoa, verbose_name='Emissor', related_name='notas_emitidas', filter=True)
    destinatario = models.ForeignKey(Pessoa, verbose_name='Destinatário', related_name='notas_recebidas', filter=True)
    valor = models.DecimalField(verbose_name='Valor')
    xml = models.FileField(verbose_name='Arquivo XML', upload_to='nfes', null=True, blank=True)

    class Meta:
        icon = 'fa-file-text-o'
        verbose_name = 'Nota Fiscal'
        verbose_name_plural = 'Notas Fiscais'
        menu = 'Fiscal::Notas Fiscais', 'fa-paw'
        list_display = 'numero', 'tipo', 'data_emissao', 'estado', 'emissor', 'destinatario', 'valor'
        can_admin = 'Gerente Financeiro'

    fieldsets = (
        ('Dados Gerais', {'fields': ('numero', ('data_emissao', 'estado'), ('emissor', 'destinatario'), 'valor', 'chave')}),
        ('Itens da Nota::Itens', {'relations': ('get_itens_viculados_almoxarifado', 'get_itens_nao_viculados_almoxarifado')}),
        ('Pagamento::Contas a Pagar', {'relations': ('get_contas_pagar',), 'condition': 'possui_contas_pagar'}),
        ('Pagamento::Contas a Receber', {'relations': ('get_contas_receber',), 'condition': 'possui_contas_receber'}),
    )

    @meta('Itens Vinculados ao Almoxarifado')
    def get_itens_viculados_almoxarifado(self):
        return self.itemnotafiscal_set.filter(produto__produto__isnull=False)

    @meta('Itens Não-Vinculados ao Almoxarifado')
    def get_itens_nao_viculados_almoxarifado(self):
        return self.itemnotafiscal_set.filter(produto__produto__isnull=True)

    @meta('Contas a Pagar')
    def get_contas_pagar(self):
        return self.contapagar_set.all()

    def possui_contas_pagar(self):
        return self.contapagar_set.exists()

    @meta('Contas a Receber')
    def get_contas_receber(self):
        return self.contareceber_set.all()

    def possui_contas_receber(self):
        return self.contareceber_set.exists()

    def __str__(self):
        return 'NFe {} - {}'.format(self.numero, self.emissor)


class ItemNotaFiscal(models.Model):
    nota_fiscal = models.ForeignKey(NotaFiscal, verbose_name='Nota Fiscal')
    produto = models.ForeignKey(ProdutoFiscal, verbose_name='Produto')
    valor_unitario = models.DecimalField(verbose_name='Valor Unitário')
    quantidade = models.DecimalField(verbose_name='Quantidade')
    valor_total = models.DecimalField(verbose_name='Valor Total')
    valor_desconto = models.DecimalField(verbose_name='Desconto')
    valor_final = models.DecimalField(verbose_name='Valor Final')

    class Meta:
        verbose_name = 'Item'
        verbose_name_plural = 'Itens'
        list_total = 'valor_final'
        list_display = 'produto', 'get_produto_almoxarifado', 'valor_unitario', 'quantidade', 'valor_total', 'valor_desconto', 'valor_final'
        can_admin = 'Gerente Financeiro'

    def __str__(self):
        return '{} {} {}'.format(self.produto, self.quantidade, self.valor_final)

    @action('Vincular ao Almoxarifado', inline=True, condition='pode_vincular_ao_almoxarifado', input='VincularProdutoAlmoxarifadoForm')
    def vincular_ao_almoxarifado(self, produto):
        self.produto.vincular_ao_almoxarifado(produto)

    def pode_vincular_ao_almoxarifado(self):
        return self.produto.produto_id is None

    @meta('Produto do Almoxarifado')
    def get_produto_almoxarifado(self):
        return self.produto.produto


class EstruturaManager(models.Manager):

    @subset('Todas')
    def all(self, *args, **kwargs):
        return super(EstruturaManager, self).all(*args, **kwargs)

    @action('Recalcular', style='ajax btn-info')
    def recalcular(self):
        for estrutura in self.all():
            estrutura.recalcular()


class Estrutura(models.Model):

    descricao = models.CharField('Descrição', null=False, blank=False, search=True)

    fieldsets = (
        ('Dados Gerais', {'fields': ('descricao', 'get_total'),}),
        ('Produtos', {'relations':('itemestrutura_set',)}),
    )

    class Meta:
        verbose_name = 'Estrutura'
        verbose_name_plural = 'Estruturas'
        menu = 'Orçamento::Estruturas'
        icon = 'fa-columns'
        add_message = 'Adicione os produtos'
        can_admin='Engenheiro'
        list_shortcut = True

    def __str__(self):
        return self.descricao

    @meta('Total')
    def get_total(self):
        return ItemEstrutura.objects.filter(estrutura=self).aggregate(Sum('total')).get('total__sum') or 0

    @action('Recalcular', category=None, style='ajax btn-info')
    def recalcular(self):
        for item_estrutura in self.itemestrutura_set.all():
            item_estrutura.save()

        self.save()

    @action('Clonar', category=None)
    def clonar(self, descricao):
        itens = self.itemestrutura_set.all()
        self.pk = None
        self.descricao = descricao
        self.save()
        for item_estrutura in itens:
            item_estrutura.pk = None
            item_estrutura.estrutura = self
            item_estrutura.save()
        self.save()
        return self


class ItemEstrutura(models.Model):

    estrutura = models.ForeignKey(Estrutura, verbose_name='Estrutura', composition=True)
    produto = models.ForeignKey(Produto, verbose_name='Produto')
    valor_unitario = models.DecimalField('Valor Unitário', exclude=True)
    quantidade = models.DecimalField('Quantidade')
    total = models.DecimalField('Total', exclude=True)

    fieldsets = (
        ('Dados Gerais', {'fields': ('estrutura', ('produto', 'quantidade'))}),
    )

    class Meta:
        verbose_name = 'Item'
        verbose_name_plural = 'Items'
        list_display = 'produto', 'valor_unitario', 'quantidade', 'total'
        can_admin='Engenheiro'

    def __str__(self):
        return 'Item %s' % self.pk

    def save(self):
        self.valor_unitario = self.produto.valor_unitario
        self.total = self.valor_unitario * self.quantidade
        super(ItemEstrutura, self).save()
        
        
class Planilha(models.Model):

    cliente = models.ForeignKey(PessoaJuridica, verbose_name='Cliente', filter=True, search=('nome', 'cnpj'))
    data = models.DateField('Data', filter=True)
    alicota_material = models.IntegerField('Alícota de Material (%)', exclude=True, default=17)
    custo_mao_de_obra = models.DecimalField('Custo com Mão de Obra', exclude=True, default=0)
    alicota_mao_de_obra = models.IntegerField('Alícota de Mão de Obra (%)', exclude=True, default=17)
    material_separado = models.BooleanField('Material Separado', exclude=True, default=False)

    fieldsets = (
        ('Dados Gerais', {'fields': ('cliente', 'data', 'material_separado')}),
        ('Estruturas', {'relations': ('estruturaplanilha_set',)}),
        ('Diversos', {'relations': ('produtoplanilha_set',)}),
        ('Resumo', {'fields': (('get_total_estrutura', 'get_total_produtos_diversos'),('alicota_material', 'alicota_mao_de_obra'), ('get_custo_materiais', 'custo_mao_de_obra'), ('get_custo_mao_obra', 'get_total_geral')), 'actions' : ('informar_custo_mao_obra',)}),
        ('Descontos', {'fields': (('get_desconto_3', 'get_desconto_5'), ('get_desconto_7', 'get_desconto_10'))}),
    )
    
    class Meta:
        verbose_name = 'Planilha'
        verbose_name_plural = 'Planilhas'
        menu = 'Orçamento::Planilhas'
        icon = 'fa-calculator'
        add_message = 'Adicione as estruturas e os produtos diversos'
        can_admin = 'Engenheiro'
        list_display = 'cliente', 'data', 'get_total_geral'
        add_shortcut = True
        list_shortcut = True
    
    def __str__(self):
        return 'Planilha Orçamentária # %s' % self.pk

    def get_nota_xml(self):
        return render_to_string('nota.xml', {'planilha':self})

    def get_dados_excel(self):
        dados = []
        itens = []
        itens.append(('#', 'ESTRUTURA', 'UND', 'PREÇO UND', 'QTD', 'TOTAL'))
        for i, item in enumerate(self.estruturaplanilha_set.all()):
            itens.append((i+1, item.estrutura.descricao, 'UND', item.valor_unitario, item.quantidade, item.total))

        itens.append(('', '', '', '', '', ''))

        itens.append(('#', 'PRODUTO', 'UND', 'PREÇO UND', 'QTD', 'TOTAL'))
        for i, item in enumerate(self.produtoplanilha_set.all()):
            itens.append((i+1, item.produto.descricao, item.produto.unidade_medida.descricao, item.valor_unitario, item.quantidade, item.total))

        materiais = []
        materiais.append(('#', 'PRODUTO', 'UND', 'PREÇO UND', 'QTD', 'TOTAL'))
        i = 1
        for produto, quantidade, unidade_medida in self.get_lista_produtos():
            materiais.append((i, produto.descricao, unidade_medida, produto.valor_unitario, quantidade, quantidade*produto.valor_unitario))
            i += 1

        estruturas = []
        estruturas.append(('ESTRUTURA', 'PRODUTO', 'UND', 'PREÇO UND', 'QTD', 'TOTAL'))
        for estrutura in Estrutura.objects.filter(pk__in=self.estruturaplanilha_set.values_list('estrutura', flat=True).order_by('estrutura').distinct()):
            for item in estrutura.itemestrutura_set.all():
                estruturas.append((item.estrutura.descricao, item.produto.descricao, item.produto.unidade_medida.descricao, item.valor_unitario, item.quantidade, item.total))

        dados.append(('Itens do Orçamento', itens))
        dados.append(('Lista de Materiais', materiais))
        dados.append(('Detalhe das Estruturas', estruturas))
        return dados

    @meta('Subtotal Estruturas (R$)')
    def get_total_estrutura(self):
        return EstruturaPlanilha.objects.filter(planilha=self).aggregate(Sum('total')).get('total__sum') or 0

    @meta('Faturamento Direto (R$)')
    def get_total_produtos_diversos(self):
        return ProdutoPlanilha.objects.filter(planilha=self).aggregate(Sum('total')).get('total__sum') or 0

    @meta('Imposto de Materiais (R$)')
    def get_custo_materiais(self):
        return self.get_total_estrutura() / (1-(Decimal(self.alicota_material)/100)) * Decimal(self.alicota_material)/100

    @action('Separar Material', condition='not material_separado')
    def separar_material(self):
        self.material_separado = True
        self.save()

    @action('Cancelar Separação de Material', condition='material_separado')
    def cancelar_separacao_material(self):
        self.material_separado = False
        self.save()

    @action('Calcular Custo Final')
    def informar_custo_mao_obra(self, alicota_material, custo_mao_de_obra, alicota_mao_de_obra):
        self.alicota_material = alicota_material
        self.alicota_mao_de_obra = alicota_mao_de_obra
        self.custo_mao_de_obra = custo_mao_de_obra
        self.save()

    @meta('Imposto da Mão de Obra (R$)')
    def get_custo_mao_obra(self):
        return self.custo_mao_de_obra / (1-(Decimal(self.alicota_mao_de_obra)/100)) * Decimal(self.alicota_mao_de_obra)/100

    @meta('Total Geral (R$)')
    def get_total_geral(self):
        if not hasattr(self, 'total_geral'):
            self.total_geral = self.get_total_estrutura() + self.get_total_produtos_diversos() + self.get_custo_materiais() + self.custo_mao_de_obra + self.get_custo_mao_obra()
        return self.total_geral

    @meta('Desconto 3%')
    def get_desconto_3(self):
        return self.get_total_geral() / Decimal('0.97')

    @meta('Desconto 5%')
    def get_desconto_5(self):
        return self.get_total_geral() / Decimal('0.95')

    @meta('Desconto 7%')
    def get_desconto_7(self):
        return self.get_total_geral() / Decimal('0.93')

    @meta('Desconto 10%')
    def get_desconto_10(self):
        return self.get_total_geral() / Decimal('0.90')

    def get_lista_produtos(self):
        produtos = dict()
        for estrutura_planilha in self.estruturaplanilha_set.all():
            for item_estrutura in estrutura_planilha.estrutura.itemestrutura_set.all():
                produto = item_estrutura.produto
                if not produto.pk in produtos:
                    produtos[produto.pk] = dict(quantidade=0, unidade_medida=produto.unidade_medida.descricao)
                produtos[produto.pk]['quantidade'] = produtos[produto.pk]['quantidade'] + item_estrutura.quantidade*estrutura_planilha.quantidade

        for produto_planinha in self.produtoplanilha_set.all():
            produto = produto_planinha.produto
            if not produto.pk in produtos:
                produtos[produto.pk] = dict(quantidade=0, unidade_medida=produto.unidade_medida.descricao)
            produtos[produto.pk]['quantidade'] = produtos[produto.pk]['quantidade'] + produto_planinha.quantidade

        lista = []
        for produto in Produto.objects.filter(pk__in=list(produtos.keys())).order_by('descricao'):
            lista.append((produto, produtos[produto.pk]['quantidade'], produtos[produto.pk]['unidade_medida']))

        return lista

    @action('Recalcular', category=None, style='ajax btn-info')
    def recalcular(self):
        for estrutura_planilha in self.estruturaplanilha_set.all():
            for item_estrutura in estrutura_planilha.estrutura.itemestrutura_set.all():
                item_estrutura.save()
            estrutura_planilha.save()

        for produto_planinha in self.produtoplanilha_set.all():
            produto_planinha.save()

        self.save()


class EstruturaPlanilha(models.Model):

    planilha = models.ForeignKey(Planilha, verbose_name='Planilha', composition=True)
    estrutura = models.ForeignKey(Estrutura, verbose_name='Estrutura')
    valor_unitario = models.DecimalField('Valor Unitário', exclude=True)
    quantidade = models.IntegerField('Quantidade')
    total = models.DecimalField('Total', exclude=True)

    produtos_faturados = models.ManyToManyField(Produto, verbose_name='Produtos Faturados', exclude=True)

    fieldsets = (
        ('Dados Gerais', {'fields': ('planilha', ('estrutura', 'quantidade'))}),
        ('Produtos Faturados', {'fields': ('produtos_faturados', )}),
    )

    class Meta:
        verbose_name = 'Estrutura'
        verbose_name_plural = 'Estruturas'
        list_display = 'estrutura', 'valor_unitario', 'quantidade', 'total'
        can_admin='Engenheiro'

    def __str__(self):
        return 'Item %s' % self.pk

    def save(self):
        self.valor_unitario = self.estrutura.get_total()
        self.total = self.valor_unitario * self.quantidade
        super(EstruturaPlanilha, self).save()


class ProdutoPlanilha(models.Model):

    planilha = models.ForeignKey(Planilha, verbose_name='Planilha', composition=True)
    produto = models.ForeignKey(Produto, verbose_name='Produto')
    valor_unitario = models.DecimalField('Valor Unitário', exclude=True)
    quantidade = models.DecimalField('Quantidade')
    faturar = models.BooleanField('Faturar?', help_text='Marque essa opção caso deseje que este item seja adicionado à nota fiscal.', default=False)
    total = models.DecimalField('Total', exclude=True)

    fieldsets = (
        ('Dados Gerais', {'fields': ('planilha', ('produto', 'quantidade'), ('valor_unitario', 'faturar'))}),
    )

    class Meta:
        verbose_name = 'Produto'
        verbose_name_plural = 'Produtos'
        list_display = 'produto', 'faturar', 'get_unidade_medida', 'valor_unitario', 'quantidade', 'total'
        can_admin='Engenheiro'

    def __str__(self):
        return 'Item %s' % self.pk

    def save(self):
        self.valor_unitario = self.produto.valor_unitario
        self.total = self.valor_unitario * self.quantidade
        super(ProdutoPlanilha, self).save()

    # inline
    @action('Alterar Valor Unitário')
    def alterar_valor_unitario(self, valor_unitario):
        ProdutoPlanilha.objects.filter(pk=self.pk).update(valor_unitario=valor_unitario, total=valor_unitario * self.quantidade)

    @meta('Und. Medida')
    def get_unidade_medida(self):
        return self.produto.unidade_medida


class NaturezaDespesa(models.Model):
    DEBITO = 1
    CREDITO = 2

    descricao = models.CharField('Descrição', null=False, blank=False, search=True)

    fieldsets = (
        ('Dados Gerais', {'fields': ('descricao',),}),
    )

    class Meta:
        verbose_name = 'Naturesa de Despesa'
        verbose_name_plural = 'Naturesas de Despesa'

    def __str__(self):
        return self.descricao


class Despesa(models.Model):

    natureza = models.ForeignKey(NaturezaDespesa, verbose_name='Natureza', null=False, blank=False, filter=False, exclude=True)

    class Meta:
        verbose_name = 'Tipo'
        verbose_name_plural = 'Tipos'

    def __str__(self):
        return self.descricao


class TipoReceita(Despesa):
    descricao = models.CharField('Descrição', null=False, blank=False, search=True)

    fieldsets = (
        ('Dados Gerais', {'fields': ('descricao', 'natureza'),}),
    )

    class Meta:
        verbose_name = 'Tipos de Receita'
        verbose_name_plural = 'Tipos de Receitas'
        menu = 'Financeiro::Cadastros::Tipos de Receitas', 'fa-usd'
        can_admin = 'Gerente Financeiro'
        icon = 'fa-usd'

    def save(self):
        natureza = NaturezaDespesa.objects.get(pk=NaturezaDespesa.CREDITO)
        self.natureza = natureza
        super(TipoReceita, self).save()


class TipoDespesa(Despesa):
    descricao = models.CharField('Descrição', null=False, blank=False, search=True)

    fieldsets = (
        ('Dados Gerais', {'fields': ('descricao', 'natureza'),}),
    )

    class Meta:
        verbose_name = 'Tipos de Despesa'
        verbose_name_plural = 'Tipos de Despesas'
        menu = 'Financeiro::Cadastros::Tipos de Despesas', 'fa-usd'
        can_admin = 'Gerente Financeiro'
        icon = 'fa-usd'

    def save(self):
        natureza = NaturezaDespesa.objects.get(pk=NaturezaDespesa.DEBITO)
        self.natureza = natureza
        super(TipoDespesa, self).save()


class FormaPagamento(models.Model):

    descricao = models.CharField('Descrição', null=False, blank=False, search=True)

    fieldsets = (
        ('Dados Gerais', {'fields': ('descricao',),}),
    )

    class Meta:
        verbose_name = 'Forma de Pagamento'
        verbose_name_plural = 'Formas de Pagamento'
        menu = 'Financeiro::Cadastros::Formas de Pagamento', 'fa-usd'
        can_admin = 'Gerente Financeiro'
        icon = 'fa-usd'

    def __str__(self):
        return self.descricao


class TipoTreinamento(models.Model):

    descricao = models.CharField('Descrição', null=False, blank=False, search=True)
    funcoes = models.ManyToManyField('gouveia.Funcao', verbose_name='Funções', filter=True, lazy=False)
    funcionarios = models.ManyToManyField('gouveia.Funcionario', verbose_name='Funcionários', blank=True, help_text='Funcionários que necessitam estar com esse tipo de treinamento em dia.')

    fieldsets = (
        ('Dados Gerais', {'fields': ('descricao',)}),
        ('Público Alvo', {'fields': ('funcoes', 'funcionarios',), }),
    )

    class Meta:
        verbose_name = 'Tipo de Treinamento'
        verbose_name_plural = 'Tipos de Treinamento'
        menu = 'Treinamento::Cadastros::Tipos de Treinamento', 'fa-mortar-board'
        can_admin = 'Administrador'
        list_display = 'descricao',

    def __str__(self):
        return self.descricao


class ContaReceberManager(models.DefaultManager):

    def total(self):
        return self.all().aggregate(Sum('valor')).get('valor__sum') or 0

    @subset('Recebidas')
    def pagas(self):
        return self.filter(data_pagamento__isnull=False)

    @subset('Não-Recebidas')
    def nao_pagas(self):
        return self.filter(data_pagamento__isnull=True)


class ContaReceber(models.Model):
    descricao = models.CharField('Descrição', null=False, blank=False, search=True)
    tipo_receita = models.ForeignKey(TipoReceita, verbose_name='Tipo', filter=True)
    pessoa = models.ForeignKey(Pessoa, verbose_name='Cliente', null=False, blank=False, filter=True)
    valor = models.MoneyField('Valor (R$)', null=False, blank=False)
    forma_pagamento = models.ForeignKey(FormaPagamento, verbose_name='Forma de Pagamento', null=False, blank=False, filter=True)
    data_solicitacao = models.DateField('Data da Solicitação', null=False, blank=False, default=datetime.today)
    data_prevista_pagamento = models.DateField('Data Prevista do Pagamento', null=False, blank=False)
    data_pagamento = models.DateField('Data do Pagamento', null=True, blank=False, exclude=True)
    valor_pago = models.MoneyField('Valor Pago (R$)', null=True, blank=False, default=0, exclude=True)
    observacao = models.TextField('Observação', null=True, blank=True)

    nota_fiscal = models.ForeignKey(NotaFiscal, verbose_name='Nota Fiscal', null=True, exclude=True)

    fieldsets = (
        ('Dados Gerais', {'fields': (('descricao', 'tipo_receita'), ('pessoa', 'obra'), ('valor', 'forma_pagamento'), ('data_solicitacao', 'data_prevista_pagamento'), 'nota_fiscal')}),
        ('Dados do Pagamento', {'fields': (('data_pagamento', 'valor_pago'),)}),
        ('Outras Informações', {'fields': ('observacao',)})
    )

    class Meta:
        verbose_name = 'Conta a Receber'
        verbose_name_plural = 'Contas a Receber'
        menu = 'Financeiro::Contas::Contas a Receber', 'fa-usd'
        list_display = 'descricao', 'valor', 'valor_pago', 'data_prevista_pagamento', 'data_pagamento'
        can_admin = 'Gerente Financeiro'

    def __str__(self):
        return self.descricao

    def registrar_recebimento_initial(self):
        return dict(valor_pago=self.valor, data_pagamento=date.today())

    @action('Registrar Recebimento', condition='not data_pagamento', inline=True)
    def registrar_recebimento(self, forma_pagamento, data_pagamento, valor_pago):
        self.forma_pagamento = forma_pagamento
        self.data_pagamento = data_pagamento
        self.valor_pago = valor_pago
        self.save()

    @action('Cancelar Recebimento', condition='data_pagamento')
    def cancelar_recebimento(self):
        self.data_pagamento = None
        self.valor_pago = 0
        self.save()


class ContaPagarManager(models.DefaultManager):

    def total(self):
        return self.all().aggregate(Sum('valor')).get('valor__sum') or 0

    @subset('Pagas')
    def pagas(self):
        return self.filter(data_pagamento__isnull=False)

    @subset('Não-Pagas')
    def nao_pagas(self):
        return self.filter(data_pagamento__isnull=True)


class ContaPagar(models.Model):
    descricao = models.CharField('Descrição', null=False, blank=False, search=True)
    tipo_despesa = models.ForeignKey(TipoDespesa, verbose_name='Tipo', filter=True)
    pessoa = models.ForeignKey(Pessoa, verbose_name='Fornecedor', null=False, blank=False, filter=True)
    valor = models.MoneyField('Valor (R$)', null=False, blank=False)
    forma_pagamento = models.ForeignKey(FormaPagamento, verbose_name='Forma de Pagamento', null=True, blank=True, filter=True)
    data_solicitacao = models.DateField('Data da Solicitação', null=False, blank=False, default=datetime.today)
    data_prevista_pagamento = models.DateField('Data Prevista do Pagamento', null=False, blank=False)
    data_pagamento = models.DateField('Data do Pagamento', null=True, blank=False, exclude=True)
    valor_pago = models.MoneyField('Valor Pago (R$)', null=True, blank=False, default=0, exclude=True)
    observacao = models.TextField('Observação', null=True, blank=True)

    nota_fiscal = models.ForeignKey(NotaFiscal, verbose_name='Nota Fiscal', null=True, exclude=True)

    fieldsets = (
        ('Dados Gerais', {'fields': (('descricao', 'tipo_despesa'), ('pessoa',), ('valor', 'forma_pagamento'), ('data_solicitacao', 'data_prevista_pagamento'), 'nota_fiscal')}),
        ('Dados do Pagamento', {'fields': (('data_pagamento', 'valor_pago'),), 'actions' : ('registrar_pagamento',)}),
        ('Outras Informações', {'fields': ('observacao',), })
    )

    class Meta:
        verbose_name = 'Conta a Pagar'
        verbose_name_plural = 'Contas a Pagar'
        menu = 'Financeiro::Contas::Contas a Pagar', 'fa-usd'
        list_display = 'descricao', 'valor', 'valor_pago', 'data_prevista_pagamento', 'data_pagamento'
        can_admin = 'Gerente Financeiro'

    def __str__(self):
        return self.descricao

    def registrar_pagamento_initial(self):
        return dict(valor_pago=self.valor, data_pagamento=date.today())

    @action('Registrar Pagamento', condition='not data_pagamento', inline=True)
    def registrar_pagamento(self, forma_pagamento, data_pagamento, valor_pago):
        self.forma_pagamento = forma_pagamento
        self.data_pagamento = data_pagamento
        self.valor_pago = valor_pago
        self.save()

    @action('Cancelar Pagamento', condition='data_pagamento')
    def cancelar_pagamento(self):
        self.data_pagamento = None
        self.valor_pago = 0
        self.save()


class Funcao(models.Model):
    descricao = models.CharField(verbose_name='Descrição', search=True)

    class Meta:
        verbose_name = 'Função'
        verbose_name_plural = 'Funções'
        menu = 'Cadastros Gerais::Funções', 'fa-th'
        can_admin = 'Administrador'

    def __str__(self):
        return self.descricao


class TipoEquipamentoManager(models.DefaultManager):
    @subset('Individuais')
    def individuais(self):
        return self.filter(individual=True)

    @subset('Coletivos')
    def coletivos(self):
        return self.filter(individual=False)


class TipoEquipamento(models.Model):
    descricao = models.CharField(verbose_name='Descrição', search=True)
    individual = models.BooleanField(verbose_name='Individual', default=True)

    class Meta:
        verbose_name = 'Tipo de Equipamento'
        verbose_name_plural = 'Tipos de Equipamento'
        menu = 'Segurança do Trabalho::Cadastros::Tipos de Equipamento', 'fa-shield'
        can_admin = 'Administrador'

    def __str__(self):
        return self.descricao


class TipoCombustivel(models.Model):
    descricao = models.CharField(verbose_name='Tipo de Combustível', search=True)

    class Meta:
        verbose_name = 'Tipo de Combustível'
        verbose_name_plural = 'Tipos de Combustível'
        menu = 'Frota::Cadastros::Tipos de Combustível', 'fa-car'
        can_admin = 'Administrador'

    def __str__(self):
        return self.descricao


class Funcionario(models.Model):

    nome = models.CharField('Nome', null=False, blank=False, search=True)
    cpf = models.CpfField(verbose_name='CPF', null=True, blank=True, search=True)

    funcao = models.ForeignKey(Funcao, verbose_name='Função', filter=True, lazy=False)
    documento = models.ImageField(verbose_name='Documento', upload_to='funcionarios', null=True, blank=True)
    assinatura = models.ImageField(verbose_name='Assinatura', null=True, blank=True)

    telefone_principal = models.PhoneField('Telefone Principal', null=True, blank=True)
    telefone_secundario = models.PhoneField('Telefone Secundário', null=True, blank=True)

    logradouro = models.CharField(verbose_name='Rua/Avenida', null=True, blank=True)
    numero = models.CharField(verbose_name='Número', null=True, blank=True)
    complemento = models.CharField(verbose_name='Complemento', null=True, blank=True)
    bairro = models.CharField(verbose_name='Bairro', null=True, blank=True)
    cep = models.CepField(verbose_name='CEP', null=True, blank=True)
    municipio = models.ForeignKey(Municipio, verbose_name='Cidade', null=True, blank=True, lazy=True)

    class Meta:
        verbose_name = 'Funcionário'
        verbose_name_plural = 'Funcionários'
        menu = 'Pessoal::Funcionários', 'fa-user'
        icon = 'fa-vcard-o'
        can_admin = 'Administrador'
        list_display = 'nome', 'cpf', 'funcao'
        list_xls = 'nome', 'cpf', 'funcao', 'telefone_principal', 'telefone_secundario'
        list_shortcut = True

    fieldsets = (
        ('Dados Gerais', {'fields': ('nome', ('cpf','funcao'), 'documento', 'assinatura')}),
        ('Contato::Telefones', {'fields': (('telefone_principal', 'telefone_secundario'),)}),
        ('Contato::Endereço', {'fields': (('logradouro', 'numero'), ('complemento', 'bairro'), ('cep', 'municipio'))}),
        ('Treinamento::Formação', {'relations': ('get_treinamentos_em_dia', 'get_treinamentos_pendentes',)}),
        ('Treinamento::Histórico de Treinamentos', {'relations': ('treinamento_set',)}),
        ('Segurança do Trabalho::Equipamentos', {'relations': ('epi_set',)}),
        ('Segurança do Trabalho::Atestados de Saude Ocupacional', {'relations': ('atestadosaudeocupacional_set',)}),
    )

    def save(self, *args, **kwargs):
        self.documento = self.cpf
        super(Funcionario, self).save(*args, **kwargs)

    def __str__(self):
        return self.nome

    @action('Informar Assinatura', can_execute='Administrador')
    def informar_assinatura(self, assinatura):
        self.assinatura = assinatura
        self.save()

    @meta('Treinamentos Necessários')
    def get_treinamentos_necessarios(self):
        return TipoTreinamento.objects.filter(funcionarios=self) | TipoTreinamento.objects.filter(funcoes=self.funcao)

    @meta('Treinamentos Em Dia')
    def get_treinamentos_em_dia(self):
        treinamentos = Treinamento.objects.filter(funcionarios=self, data_validade__gte=date.today())
        return self.get_treinamentos_necessarios().filter(
            id__in=treinamentos.values_list('tipo', flat=True)
        )

    @meta('Treinamentos Pendentes')
    def get_treinamentos_pendentes(self):
        treinamentos = Treinamento.objects.filter(funcionarios=self, data_validade__gte=date.today())
        return self.get_treinamentos_necessarios().exclude(
            id__in=treinamentos.values_list('tipo', flat=True)
        )


class TreinamentoManager(models.DefaultManager):

    @subset('Vencimento em até 15 dias', can_notify='Administrador')
    def prestes_vencer(self):
        pass

    @subset('Vencimento em até 15 dias', can_notify='Administrador')
    def prestes_vencer(self):
        hoje = date.today()
        data = add_days(hoje, 15)
        return self.filter(data_validade__lte=data, data_validade__gte=hoje)


class Treinamento(models.Model):
    tipo = models.ForeignKey(TipoTreinamento, verbose_name='Tipo de Treinamento', filter=True, lazy=False)
    funcionarios = models.ManyToManyField(Funcionario, verbose_name='Participantes', filter=True, lazy=False)
    data_realizacao = models.DateField(verbose_name='Data', default=datetime.today, filter=True)
    data_validade = models.DateField(verbose_name='Validade', filter=True)

    fieldsets = (
        ('Dados Gerais', {'fields': ('tipo', 'funcionarios'),}),
        ('Datas', {'fields': ('data_realizacao', 'data_validade'), }),
    )

    class Meta:
        verbose_name = 'Treinamento'
        verbose_name_plural = 'Treinamentos'
        can_admin = 'Administrador'
        menu = 'Treinamento::Treinamentos', 'fa-mortar-board'
        icon = 'fa-mortar-board'

    def __str__(self):
        return '{}'.format(self.tipo)


class EquipamentoProtecaoManager(models.DefaultManager):

    @subset('Em Utilização')
    def em_utilizacao(self):
        return self.filter(data_baixa__isnull=True)

    @subset('Vencimento em até 15 dias')
    def prestes_vencer(self):
        hoje = date.today()
        data = add_days(hoje, 15)
        return self.em_utilizacao().filter(data_validade__gt=hoje, data_validade__lte=data)

    @subset('Vencidos')
    def vencidos(self):
        return self.em_utilizacao().filter(data_validade__lte=date.today())

    @subset('Devolvidos')
    def devolvidos(self):
        return self.filter(data_baixa__isnull=False)


class EquipamentoProtecao(models.Model):
    tipo_equipamento = models.ForeignKey(TipoEquipamento, verbose_name='Tipo de Equipamento', filter=True)

    numero_ca = models.CharField(verbose_name='Nº do CA', null=True, blank=True)

    data_compra = models.DateField(verbose_name='Data da Compra', null=True, blank=True)
    data_validade = models.DateField(verbose_name='Data de Validade', null=True, blank=True)

    observacao = models.TextField(verbose_name='Observação', null=True, blank=True)

    data_baixa = models.DateField(verbose_name='Data da Devolução', exclude=True, null=True, blank=True, help_text='Data em que o equipamento deixou de ser utilizado')
    motivo_baixa = models.TextField(verbose_name='Motivo da Devolução', null=True, blank=True, exclude=True, help_text='Ex: Quebrou, perdeu, se venceu, etc.')

    def __str__(self):
        return '{} - {}'.format(self.id, self.tipo_equipamento)

    @action('Registrar Devolução', can_execute='Administrador', subset=('em_utilizacao', 'prestes_vencer', 'vencidos'), condition='not data_baixa')
    def dar_baixa(self, data_baixa, motivo_baixa):
        self.data_baixa = data_baixa
        self.motivo_baixa = motivo_baixa
        self.save()


class EPIManager(EquipamentoProtecaoManager):
    pass


class EPI(EquipamentoProtecao):
    funcionario = models.ForeignKey(Funcionario, verbose_name='Funcionário', filter=True)
    quantidade = models.IntegerField(verbose_name='Quantidade')
    data_entrega = models.DateField(verbose_name='Data da Entrega')

    fieldsets = (
        (u'Dados Gerais', {'fields': (('funcionario', 'data_entrega'), ('tipo_equipamento', 'quantidade'))}),
        (u'Datas/Validade', {'fields': ('numero_ca', ('data_compra', 'data_validade'))}),
        (u'Dados da Devolução', {'fields': ('data_baixa', 'motivo_baixa')}),
        (u'Outros Dados', {'fields': ('observacao',)}),
    )

    class Meta:
        verbose_name = 'EPI'
        verbose_name_plural = 'EPIs'
        icon = 'fa-shield'
        menu = 'Segurança do Trabalho::Equipamentos de Proteção::EPI'
        can_admin = 'Administrador'
        list_display = 'funcionario', 'tipo_equipamento', 'quantidade', 'data_compra', 'data_entrega', 'data_validade', 'data_baixa'
        list_xls = 'tipo_equipamento', 'funcionario', 'quantidade', 'observacao', 'data_baixa', 'motivo_baixa'

    def choices(self):
        return dict(tipo_equipamento=TipoEquipamento.objects.filter(individual=True))

    def __str__(self):
        return '{} - {} - {}'.format(self.id, self.tipo_equipamento, self.funcionario)


class EPCManager(EquipamentoProtecaoManager):
    pass


class EPC(EquipamentoProtecao):
    quantidade = models.IntegerField(verbose_name='Quantidade')

    fieldsets = (
        (u'Dados Gerais', {'fields': (('tipo_equipamento', 'quantidade'),)}),
        (u'Datas/Validade', {'fields': ('numero_ca', ('data_compra', 'data_validade'))}),
        (u'Dados da Devolução', {'fields': ('data_baixa', 'motivo_baixa')}),
        (u'Outros Dados', {'fields': ('observacao',)}),
    )

    class Meta:
        verbose_name = 'EPC'
        verbose_name_plural = 'EPCs'
        icon = 'fa-shield'
        menu = 'Segurança do Trabalho::Equipamentos de Proteção::EPC'
        can_admin = 'Administrador'
        list_display = 'tipo_equipamento', 'quantidade', 'data_compra', 'data_validade', 'data_baixa'
        list_xls = 'tipo_equipamento', 'funcionario', 'quantidade', 'observacao', 'data_baixa', 'motivo_baixa'

    def choices(self):
        return dict(tipo_equipamento=TipoEquipamento.objects.filter(individual=False))


class VeiculoManager(models.DefaultManager):

    @subset('Aguardando Revisão', can_notify='Administrador')
    def aquardando_revisao(self):
        pks = []
        for veiculo in self:
            ultima_revisao = veiculo.revisaoveicular_set.order_by('-data').first()
            if ultima_revisao and ultima_revisao.data_proxima_revisao:
                if ultima_revisao.data_proxima_revisao < date.today():
                    pks.append(veiculo.pk)
                quilometragem_atual = veiculo.get_quilometragem()
                if quilometragem_atual and quilometragem_atual >= ultima_revisao.quilometragem_proxima_revisao:
                    pks.append(veiculo.pk)
        return self.filter(id__in=pks)


class Veiculo(models.Model):
    descricao = models.CharField(verbose_name='Descrição', search=True)
    placa = models.MercosulCarPlateField(verbose_name='Placa', search=True)
    observacao = models.TextField(verbose_name='Observação', null=True, blank=True)
    documento = models.ImageField(verbose_name='Documento', upload_to='veiculos', null=True, blank=True)

    fieldsets = (
        (u'Dados Gerais', {'fields': (('descricao', 'placa'), 'get_quilometragem', 'documento')}),
        (u'Observação', {'fields': ('observacao',)}),
        (u'Viagens::Viagens', {'relations': ('viagem_set',)}),
        (u'Abastecimentos::Abastecimentos', {'relations': ('ordemabastecimento_set',)}),
        (u'Revisões::Revisões', {'relations': ('revisaoveicular_set',)}),
    )

    class Meta:
        verbose_name = 'Veículo'
        verbose_name_plural = 'Veículos'
        icon = 'fa-car'
        menu = 'Frota::Veículos', 'fa-car'
        can_admin = 'Administrador'
        list_display = 'descricao', 'placa', 'get_quilometragem', 'documento'
        list_shortcut = True
        list_xls = 'descricao', 'placa', 'observacao'

    def __str__(self):
        return '{} - {}'.format(self.descricao, self.placa)

    @meta('Quilometragem')
    def get_quilometragem(self):
        ultima_viagem = self.viagem_set.filter(quilometragem_chegada__isnull=False).order_by('-quilometragem_chegada').first()
        return ultima_viagem and ultima_viagem.quilometragem_chegada or None

    @meta('Nível do Combustível')
    def get_nivel_combustivel(self):
        ultima_viagem = self.viagem_set.filter(quilometragem_chegada__isnull=False, nivel_combustivel_chegada__isnull=False).order_by('-quilometragem_chegada').first()
        return ultima_viagem and ultima_viagem.nivel_combustivel_chegada or None


class RevisaoVeicular(models.Model):
    veiculo = models.ForeignKey(Veiculo, verbose_name='Veículo', filter=True, lazy=False)
    data = models.DateField(verbose_name='Data')
    quilometragem = models.IntegerField(verbose_name='Kilometragem')

    data_proxima_revisao = models.DateField(verbose_name='Data da Próxima Revisão', null=True, blank=True)
    quilometragem_proxima_revisao = models.IntegerField(verbose_name='Kilometragem da Próxima Revisão')

    observacao = models.TextField(verbose_name=u'Observação', null=True, blank=True)

    fieldsets = (
        (u'Dados Gerais', {'fields': ('veiculo', ('data', 'quilometragem'))}),
        (u'Próxima Revisão', {'fields': (('data_proxima_revisao', 'quilometragem_proxima_revisao'),)}),
        (u'Outras Informações', {'fields': ('observacao',)})
    )

    class Meta:
        verbose_name = 'Revisão'
        verbose_name_plural = 'Revisões'
        can_admin = 'Administrador'
        icon = 'fa-gears'
        menu = 'Frota::Revisões'
        list_xls = 'veiculo', 'data', 'quilometragem', 'data_proxima_revisao', 'quilometragem_proxima_revisao'

    def __str__(self):
        return 'Revisão {}'.format(self.pk)


class ViagemManager(models.DefaultManager):

    @subset('Em Andamento', can_notify='Administrador')
    def em_andamento(self):
        return self.filter(data_hora_chegada__isnull=True)

    @subset('Finalizadas')
    def finalizadas(self):
        return self.filter(data_hora_chegada__isnull=False)


class OrdemAbastecimentoManager(models.DefaultManager):

    @subset('Aguardando Finalização', can_notify='Administrador')
    def em_andamento(self):
        return self.filter(tipo_combustivel__isnull=True)


class OrdemAbastecimento(models.Model):
    numero = models.CharField(verbose_name='Número')
    veiculo = models.ForeignKey(Veiculo, verbose_name='Veículo', filter=True, lazy=False)
    responsavel = models.ForeignKey(Funcionario, verbose_name='Responsável', filter=True, lazy=False)
    data_hora_partida = models.DateTimeField(verbose_name='Data/Hora da Partida')
    
    tipo_combustivel = models.ForeignKey(TipoCombustivel, verbose_name='Tipo de Combustível', null=True, exclude=True)
    valor_unitario = models.DecimalField(verbose_name='Valor Litro', null=True, exclude=True)
    quantidade = models.DecimalField(verbose_name='Quantidade', null=True, exclude=True)
    valor_total = models.DecimalField(verbose_name='Valor Total', null=True, exclude=True)
    data_hora_chegada = models.DateTimeField(verbose_name='Data/Hora da Chegada', null=True, exclude=True)
    cupom_fiscal = models.CharField(verbose_name='Cupom Fiscal', null=True, blank=True, exclude=True)

    class Meta:
        verbose_name = 'Ordem de Abastecimento'
        verbose_name_plural = 'Ordens de Abastecimento'
        menu = 'Frota::Ordens de Abastecimento'
        can_admin = 'Administrador'
        list_display = 'numero', 'veiculo', 'responsavel', 'data_hora_partida', 'valor_total', 'cupom_fiscal'

    @action('Finalizar', can_execute='Administrador', inline=True, subset=('em_andamento',), condition='not tipo_combustivel')
    def finalizar(self, tipo_combustivel, valor_unitario, quantidade, data_hora_chegada, cupom_fiscal):
        self.tipo_combustivel = tipo_combustivel
        self.valor_unitario = valor_unitario
        self.quantidade = quantidade
        self.data_hora_chegada = data_hora_chegada
        self.cupom_fiscal = cupom_fiscal
        self.valor_total = self.quantidade * self.valor_unitario
        self.save()


class Viagem(models.Model):

    NIVEL_COMBUSTIVEL_CHOICE = [[x*10, '{}%'.format(x*10)] for x in range(1, 11)]

    veiculo = models.ForeignKey(Veiculo, verbose_name='Veículo', filter=True, lazy=False)
    responsavel = models.ForeignKey(Funcionario, verbose_name='Responsável', filter=True, lazy=False)

    data_hora_saida = models.DateTimeField(verbose_name='Data/Hora da Saída', default=datetime.now, filter=True)
    quilometragem_saida = models.IntegerField(verbose_name='Kilometragem da Saída', null=True, blank=True)
    nivel_combustivel_saida = models.IntegerField(verbose_name='Nível do Combustível da Saída', null=True, blank=True, choices=NIVEL_COMBUSTIVEL_CHOICE)

    data_hora_chegada = models.DateTimeField(verbose_name='Data/Hora da Chegada', filter=True, null=True, exclude=True)
    quilometragem_chegada = models.IntegerField(verbose_name='Kilometragem da Chegada', null=True, exclude=True, blank=True)
    nivel_combustivel_chegada = models.IntegerField(verbose_name='Nível do Combustível da Chegada', null=True, blank=True, exclude=True, choices=NIVEL_COMBUSTIVEL_CHOICE)

    observacao = models.TextField(verbose_name=u'Observação', null=True, blank=True)

    fieldsets = (
        (u'Dados Gerais', {'fields': (('veiculo', 'responsavel'), )}),
        (u'Dados da Saída', {'fields': (('data_hora_saida', 'quilometragem_saida', 'nivel_combustivel_saida'),)}),
        (u'Dados da Chegada', {'fields': (('data_hora_chegada', 'quilometragem_chegada', 'nivel_combustivel_chegada'),)}),
        (u'Observação', {'fields': ('observacao', )}),
    )

    class Meta:
        verbose_name = 'Viagem'
        verbose_name_plural = 'Viagens'
        icon = 'fa-retweet'
        can_admin = 'Administrador'
        menu = 'Frota::Viagens'
        add_shortcut = True
        verbose_female = True
        list_shortcut = True
        list_xls = 'id', 'veiculo__placa', 'veiculo__descricao', 'responsavel', 'data_hora_saida', 'quilometragem_saida', 'nivel_combustivel_saida', 'data_hora_chegada', 'quilometragem_chegada', 'nivel_combustivel_chegada', 'observacao'

    def __str__(self):
        return 'Viagem {}'.format(self.pk)

    def save(self, *args, **kwargs):
        if self.veiculo_id:
            if not self.pk and Viagem.objects.filter(veiculo=self.veiculo, data_hora_chegada__isnull=True).exists():
                raise ValidationError('Finalize a viagem em andamento para esse veículo')
            quilometragem_atual = self.veiculo.get_quilometragem()
            if False and quilometragem_atual and self.quilometragem_saida < quilometragem_atual:
                raise ValidationError('A quilometragem de saída deve ser maior ou igual que {}'.format(quilometragem_atual))
        super(Viagem, self).save(*args, **kwargs)

    def initial(self):
        if self.veiculo_id:
            return dict(quilometragem_saida=self.veiculo.get_quilometragem(), nivel_combustivel_saida=self.veiculo.get_nivel_combustivel())
        return {}

    def finalizar_initial(self):
        return dict(data_hora_chegada=datetime.now(), quilometragem_chegada=self.quilometragem_saida, nivel_combustivel_chegada=self.nivel_combustivel_saida)

    @action('Finalizar', can_execute='Administrador', subset='em_andamento', condition='not data_hora_chegada')
    def finalizar(self, data_hora_chegada, quilometragem_chegada, nivel_combustivel_chegada, observacao):
        if self.quilometragem_saida == quilometragem_chegada:
            raise ValidationError('A quilometragem da chegada deve ser maior do que a da saída')
        self.data_hora_chegada = data_hora_chegada
        self.quilometragem_chegada = quilometragem_chegada
        self.nivel_combustivel_chegada = nivel_combustivel_chegada
        self.observacao = observacao
        self.save()

    def is_km_chegada_errada(self):
        return self.quilometragem_saida is None or self.quilometragem_chegada and self.quilometragem_chegada <= self.quilometragem_saida

    @action('Corrigir Kilometragem da Chegada ', inline=True)
    def corrigir_km_chegada(self, quilometragem_chegada):
        self.quilometragem_chegada = quilometragem_chegada
        self.save()


class AtestadoSaudeOcupacional(models.Model):
    funcionario = models.ForeignKey(Funcionario, verbose_name='Funcionário')
    funcoes = models.TextField(verbose_name='Funções', search=True)
    data_validade = models.DateField(verbose_name='Data de Validade')

    class Meta:
        verbose_name = 'Atestado de Saúde Ocupacional'
        verbose_name_plural = 'Atestados de Saúde Ocupacional'
        can_admin = 'Administrador'
        menu = 'Segurança do Trabalho::Atestados de Saúde Ocupacional'

    def __str__(self):
        return 'ASO {}'.format(self.id)
