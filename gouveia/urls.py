# -*- coding: utf-8 -*-

from django.conf.urls import url, include
from djangoplus.admin import views

urlpatterns = [
    url(r'^$', views.public),
    url(r'^admin/$', views.index),
    url(r'', include('djangoplus.admin.urls')),
]