# -*- coding: utf-8 -*-

from django.conf import settings
from djangoplus.test import TestCase
from djangoplus.admin.models import User
from djangoplus.test.decorators import testcase


class AppTestCase(TestCase):

    def test(self):
        User.objects.create_superuser('000.000.000-00', None, settings.DEFAULT_PASSWORD)
        self.execute_flow()

    @testcase('', username='000.000.000-00')
    def cadastrar_engenheiro(self):
        self.click_menu('Usuários', 'Engenheiros')
        self.click_button('Cadastrar')
        self.enter('CPF', '047.704.024-14')
        self.enter('Nome', 'Carlos Breno')
        self.enter('E-mail', 'brenokcc@yahoo.com.br')
        self.enter('Telefone Principal', '')
        self.enter('Telefone Secundário', '')
        self.click_button('Salvar')

    @testcase('', username='047.704.024-14')
    def cadastrar_pessoajuridica(self):
        self.click_menu('Pessoal', 'Pessoas Jurídicas')
        self.click_button('Cadastrar')
        self.enter('CNPJ', '65.849.173/0001-24')
        self.enter('Nome', 'Cosern')
        self.enter('Telefone Principal', '')
        self.enter('Telefone Secundário', '')
        self.enter('Contato', 'Pedro')
        self.click_button('Salvar')

    @testcase('', username='047.704.024-14')
    def cadastrar_unidademedida(self):
        self.click_menu('Cadastros', 'Unidades de Medida')
        self.click_button('Cadastrar')
        self.enter('Descrição', 'M')
        self.click_button('Salvar')

    @testcase('', username='047.704.024-14')
    def cadastrar_produto(self):
        self.click_menu('Estoque', 'Produtos')
        self.click_button('Cadastrar')
        self.enter('Descrição', 'Fio')
        self.choose('Unidade de Medida', 'M')
        self.enter('Valor Unitário', '1,00')
        self.click_button('Salvar')

    @testcase('', username='047.704.024-14')
    def cadastrar_estrutura(self):
        self.click_menu('Estruturas')
        self.click_button('Cadastrar')
        self.enter('Descrição', 'Estrutura Padrão')
        self.click_button('Salvar')

        self.look_at_panel('Produtos')
        self.click_button('Adicionar Item')
        self.look_at_popup_window()
        self.choose('Produto', 'Fio')
        self.enter('Quantidade', '1')
        self.click_button('Salvar')

    @testcase('', username='047.704.024-14')
    def cadastrar_planilha(self):
        self.click_menu('Planilhas')
        self.click_button('Cadastrar')
        self.choose('Cliente', 'Cosern')
        self.enter('Data', '01/01/2017')
        self.click_button('Salvar')

        self.look_at_panel('Estruturas')
        self.click_button('Adicionar Estrutura')
        self.look_at_popup_window()
        self.choose('Estrutura', 'Padrão')
        self.enter('Quantidade', '1')
        self.click_button('Salvar')

        self.look_at_panel('Diversos')
        self.click_button('Adicionar Produto')
        self.look_at_popup_window()
        self.choose('Produto', 'Fio')
        self.enter('Quantidade', '1')
        self.click_button('Salvar')

    @testcase('', username='047.704.024-14')
    def informar_custo_mao_obra(self):
        self.open('/list/gouveia/planilha/')
        self.click_icon('Visualizar')
        self.click_button('Calcular Custo Final')
        self.look_at_popup_window()
        self.enter('Alícota de Material (%)', '10')
        self.enter('Custo com Mão de Obra', '100,00')
        self.enter('Alícota de Mão de Obra (%)', '10')
        self.click_button('Calcular Custo Final')

    @testcase('', username='000.000.000-00')
    def recalcular(self):
        self.click_menu('Planilhas')
        self.click_icon('Visualizar')
        self.click_button('Recalcular')
