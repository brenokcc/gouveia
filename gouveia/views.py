# -*- coding: utf-8 -*-

from djangoplus.ui.components.navigation.breadcrumbs import httprr
from djangoplus.ui.components.utils import Chart
from djangoplus.ui.components.calendar import ModelCalendar
from gouveia.models import *
from gouveia.forms import *
from djangoplus.decorators.views import view, action, dashboard
from djangoplus.utils.http import XlsResponse, PdfResponse


@action(Planilha, 'Listar Materiais', style='pdf ajax')
def lista_material(request, pk):
    planilha = Planilha.objects.get(pk=pk)
    return locals()


@action(Planilha, 'Exportar Excel', category=None, style='btn-success')
def exportar_planilha(request, pk):
    planilha = Planilha.objects.get(pk=pk)
    dados = planilha.get_dados_excel()
    return XlsResponse(dados, planilha.cliente.nome)


# @action(Planilha, 'Visualizar Nota', style='popup', category='Faturamento')
def visualizar_nota(request, pk):
    planilha = Planilha.objects.get(pk=pk)
    xml = planilha.get_nota_xml()
    return locals()


@dashboard(can_view='Gerente Financeiro')
def contas(request):
    qs_contas_pagar = ContaPagar.objects.filter(data_pagamento__isnull=True)
    qs_contas_receber = ContaReceber.objects.filter(data_pagamento__isnull=True)

    widget = ModelCalendar(request, 'Contas Pendentes', True)
    widget.add(qs_contas_pagar, 'data_prevista_pagamento', color='#fc8675', action_names=['registrar_pagamento'])
    widget.add(qs_contas_receber, 'data_prevista_pagamento', color='#65cea7', action_names=['registrar_recebimento'])

    return locals()


# @action(Planilha, 'Identificar Produtos', category='Faturamento')
def identificar_produtos_faturados(request, pk):
    title = 'Configurar Faturamento'
    planilha = Planilha.objects.get(pk=pk)
    form = IdentificarProdutosFaturadosForm(request, instance=planilha)
    if form.is_valid():
        form.save()
        return httprr(request, '..', 'Produtos identificados com sucesso')
    return locals()


@view('Relatório', can_view='Gerente Financeiro', icon='fa-bar-chart-o', menu='Financeiro::Relatório')
def relatorio_financeiro(request):
    form = RelatorioForm(request)
    if form.is_valid():
        efetivado, data_inicio, data_fim = form.processar()

        if efetivado:
            qs_contas_pagar = ContaPagar.objects.filter(data_pagamento__isnull=False, data_pagamento__gte=data_inicio, data_pagamento__lte=data_fim)
            qs_contas_receber = ContaReceber.objects.filter(data_pagamento__isnull=False, data_pagamento__gte=data_inicio, data_pagamento__lte=data_fim)
            attr = 'valor_pago'
        else:
            qs_contas_pagar = ContaPagar.objects.filter(data_prevista_pagamento__gte=data_inicio, data_prevista_pagamento__lte=data_fim)
            qs_contas_receber = ContaReceber.objects.filter(data_prevista_pagamento__gte=data_inicio, data_prevista_pagamento__lte=data_fim)
            attr = 'valor'

        contas_pagar = qs_contas_pagar.sum(attr, 'tipo_despesa', 'forma_pagamento')
        contas_receber = qs_contas_receber.sum(attr, 'tipo_receita', 'forma_pagamento')
        tabela_contas_pagar = contas_pagar.as_table(request)
        tabela_contas_pagar.title = 'Contas a Pagar por Tipo e Forma de Pagamento'
        tabela_contas_receber = contas_receber.as_table(request)
        tabela_contas_receber.title = 'Contas a Receber por Tipo e Forma de Pagamento'
        grafico_contas_pagar = contas_pagar.as_chart(request)
        grafico_contas_pagar.title = 'Contas a Pagar por Tipo e Forma de Pagamento'
        grafico_contas_receber = contas_receber.as_chart(request)
        grafico_contas_receber.title = 'Contas a Receber por Tipo e Forma de Pagamento'

        despesa = qs_contas_pagar.aggregate(Sum(attr)).get('%s__sum' % attr) or 0
        receita = qs_contas_receber.aggregate(Sum(attr)).get('%s__sum' % attr) or 0
        balancete_geral = Chart(request, labels=['Despesa', 'Receita'], series=[[float(despesa), float(receita)]], symbol='R$', title='Balancete').donut()
        contas_pagar_por_tipo = qs_contas_pagar.sum(attr, 'tipo_despesa')
        contas_receber_por_tipo = qs_contas_receber.sum(attr, 'tipo_receita')
        tabela_despesas_por_tipo = contas_pagar_por_tipo.as_table(request)
        tabela_despesas_por_tipo.title = 'Despesas por Tipo'
        tabela_receitas_por_tipo = contas_receber_por_tipo.as_table(request)
        tabela_receitas_por_tipo.title = 'Receitas por Tipo'
        grafico_despesas_por_tipo = contas_pagar_por_tipo.as_chart(request)
        grafico_despesas_por_tipo.title = 'Despesas por Tipo'
        grafico_receitas_por_tipo = contas_receber_por_tipo.as_chart(request)
        grafico_receitas_por_tipo.title = 'Receitas por Tipo'
    return locals()


@view('Relatório da Frota', menu='Frota::Relatório')
def relatorio_frota(request):
    title = 'Relatório da Frota'
    form = RelatorioFrotaForm(request)
    if form.is_valid():
        panel = form.processar()
    return locals()


@action(Treinamento, 'Relatório de Treinamento por Funcionário', category='Emitir Relatório', style='pdf', inline=True, icon='fa-print', condition='conteudo')
def relatorio_treinamento_por_funcionario(request):
    pks = TipoTreinamento.objects.all().values_list('funcionarios', flat=True).distinct()
    funcionarios = Funcionario.objects.filter(pk__in=pks)
    return locals()


@action(Planilha, 'Atualizar Preços', category=None)
def atualizar_precos(request, pk):
    instance = Planilha.objects.get(pk=pk)
    form = AtualizarPrecosForm(request, instance=instance)
    if form.is_valid():
        form.processar()
        return httprr(request, '..', 'Preços atualizados com sucesso')
    return locals()
