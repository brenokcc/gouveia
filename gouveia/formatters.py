# -*- coding: utf-8 -*-
from djangoplus.decorators.formatters import formatter


def status(cor, descricao):
    style = 'background-color:{};text-align:center;height:30px;width:150px;padding:5px;color:#FFF;float:left'.format(
        cor
    )
    html = '<div class="colorPickSelector" style="{}">{}</div>'.format(style, descricao)
    return html


@formatter()
def status_etapa(etapa):
    if etapa:
        return status(etapa.cor, etapa.descricao)
    else:
        return status('#CCC', 'Rascunho')


@formatter()
def color(cor):
    return status(cor, '')


@formatter()
def conteudo_proposta(conteudo):
    return '<div class="panel panel-default" id="panel-dados-gerais"><div class="panel-heading">Conteúdo</div><div class="panel-body">{}</div></div>'.format(conteudo)