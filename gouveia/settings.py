# -*- coding: utf-8 -*-

from djangoplus.conf.base_settings import *
from os.path import abspath, dirname, join, exists
from os import sep

BASE_DIR = abspath(dirname(dirname(__file__)))
PROJECT_NAME = __file__.split(sep)[-2]

STATIC_ROOT = join(BASE_DIR, 'static')
MEDIA_ROOT = join(BASE_DIR, 'media')

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': join(BASE_DIR, 'sqlite.db'),
        'USER': '',
        'PASSWORD': '',
        'HOST': '',
        'PORT': '',
    }
}

WSGI_APPLICATION = '%s.wsgi.application' % PROJECT_NAME

INSTALLED_APPS += (
    PROJECT_NAME,
    'endless',
)

ROOT_URLCONF = '%s.urls' % PROJECT_NAME

if exists(join(BASE_DIR, 'logs')):
    DEBUG = False
    ALLOWED_HOSTS = ['*']

    SERVER_EMAIL = 'root@djangoplus.net'
    ADMINS = [('Admin', 'root@djangoplus.net')]
else:
    EMAIL_BACKEND = 'django.core.mail.backends.filebased.EmailBackend'
    EMAIL_FILE_PATH = join(BASE_DIR, 'mail')

EXTRA_JS = ['/static/gouveia.js']
USERNAME_MASK = '000.000.000-00'

DIGITAL_OCEAN_SERVER = '104.236.79.20'
DIGITAL_OCEAN_DOMAIN = 'gouveia.djangoplus.net'
DIGITAL_OCEAN_TOKEN = 'f0e9844cdb9e0dc3705c13ff84c83d65593538b04a3869b1df4b567603f2bc9e'

