# -*- coding: utf-8 -*-
import datetime
from djangoplus.ui.components import forms
from djangoplus.ui.components.utils import Table

from gouveia.models import Produto, Planilha, Veiculo


class IdentificarProdutosFaturadosForm(forms.ModelForm):

    class Meta:
        title = 'Configurar Faturamento'
        submit_label = 'Salvar'
        model = Planilha
        note = 'Informe os produtos que serão faturados'
        fields = ()

    def __init__(self, *args, **kwargs):
        super(IdentificarProdutosFaturadosForm, self).__init__(*args, **kwargs)
        self.fieldsets = []
        for estrutura_planilha in self.instance.estruturaplanilha_set.all().order_by('pk'):
            field_name = estrutura_planilha.pk
            pks = estrutura_planilha.estrutura.itemestrutura_set.values_list('produto', flat=True)
            pks_selecionados = estrutura_planilha.produtos_faturados.values_list('pk', flat=True)
            produtos = Produto.objects.filter(pk__in=pks)
            self.fields[field_name] = forms.MultipleModelChoiceField(produtos,
                                                        widget=forms.widgets.RenderableSelectMultiple(),
                                                        label = '', required= False,
                                                        initial=not self.request.POST and pks_selecionados or None)

            self.fieldsets.append((estrutura_planilha.estrutura.descricao, {'fields': (field_name,)}))

    def save(self, *args, **kwargs):
        super(IdentificarProdutosFaturadosForm, self).save(*args, **kwargs)
        for estrutura_planilha in self.instance.estruturaplanilha_set.all().order_by('pk'):
            estrutura_planilha.produtos_faturados.clear()
        for field_name in self.data:
            if field_name.isdigit():
                estrutura_planilha = self.instance.estruturaplanilha_set.get(pk=field_name)
                for produto in Produto.objects.filter(pk__in=self.request.POST.getlist(field_name)):
                    estrutura_planilha.produtos_faturados.add(produto)


class RelatorioForm(forms.Form):

    class Meta:
        submit_label = 'Gerar Relatório'
        title = 'Informe o Período'

    data_inicio = forms.DateField(label='Data de Início')
    data_fim = forms.DateField(label='Data de Fim')
    efetivado = forms.BooleanField(label='Apenas Efetivados', required=False, help_text='Considerar apenas as contas quitadas e seu respectivos valores pagos/recebidos.')

    fieldsets = (
        ('Filtros', {'fields': (('data_inicio', 'data_fim'), 'efetivado')}),
    )

    def processar(self):
        return self.cleaned_data['efetivado'], self.cleaned_data['data_inicio'], self.cleaned_data['data_fim']


class RelatorioFrotaForm(forms.Form):
    data_inicio = forms.DateField(label='Data de Início', required=False)
    data_fim = forms.DateField(label='Data de Fim', required=False)
    veiculo = forms.ModelChoiceField(Veiculo.objects.all(), label='Veículo', required=False)

    fieldsets = (
        ('Filtros', {'fields': (('data_inicio', 'data_fim'), 'veiculo')}),
    )

    def processar(self):
        header = ('Veículo', 'Abastecimentos', 'Tipos de Combustível', 'Total Abastecido (R$)', 'Qtd de Viagens', 'Km Rodados')
        rows = []
        veiculo = self.cleaned_data['veiculo']
        for veiculo in veiculo and [veiculo] or Veiculo.objects.all():
            qs_viagem = veiculo.viagem_set.all()
            qs_ordem_abastecimento = veiculo.ordemabastecimento_set.all()
            data_inicio = self.cleaned_data['data_inicio']
            data_fim = self.cleaned_data['data_fim']
            if data_inicio:
                data_inicio = datetime.datetime(data_inicio.year, data_inicio.month, data_inicio.day, 0, 0, 0)
                qs_ordem_abastecimento = qs_ordem_abastecimento.filter(data_hora_chegada__gte=data_inicio)
                qs_viagem = qs_viagem.filter(data_hora_saida__gte=data_inicio)
            if data_fim:
                data_fim = datetime.datetime(data_fim.year, data_fim.month, data_fim.day, 23, 59, 59)
                qs_ordem_abastecimento = qs_ordem_abastecimento.filter(data_hora_chegada__lte=data_fim)
                qs_viagem = qs_viagem.filter(data_hora_saida__lte=data_fim)
            qtd_abasteciento = qs_ordem_abastecimento.count()
            tipos_combustivel = qs_ordem_abastecimento.filter(tipo_combustivel__isnull=False).values_list('tipo_combustivel__descricao', flat=True).distinct()
            total_abastecido = qs_ordem_abastecimento.sum('valor_total')
            qtd_viagens = qs_viagem.count()
            km_rodado = 0
            for quilometragem_saida, quilometragem_chegada in qs_viagem.values_list('quilometragem_saida', 'quilometragem_chegada'):
                if quilometragem_saida and quilometragem_chegada:
                    km_rodado += quilometragem_chegada - quilometragem_saida
            rows.append((veiculo, qtd_abasteciento, tipos_combustivel, total_abastecido, qtd_viagens, km_rodado))
        table = Table(self.request, 'Relatório', header, rows)
        return table


class ImportarNotaFiscal(forms.Form):
    arquivo_xml = forms.FileField(label='Arquivo XML')
    gerar_contas = forms.BooleanField(label='Gerar Contas Pagar/Receber', required=False)

    class Meta:
        submit_label = 'Importar XML'
        title = 'Importar Nota Fiscal'


class AtualizarPrecosForm(forms.ModelForm):

    class Meta:
        title = 'Atualizar Preços'
        submit_label = 'Salvar'
        model = Planilha
        fields = ()

    def __init__(self, *args, **kwargs):
        super(AtualizarPrecosForm, self).__init__(*args, **kwargs)
        ids = []
        for estrutura_planilha in self.instance.estruturaplanilha_set.all():
            ids.extend(estrutura_planilha.estrutura.itemestrutura_set.values_list('produto_id', flat=True))
        ids.extend(self.instance.produtoplanilha_set.values_list('produto_id', flat=True))
        for produto in Produto.objects.filter(id__in=set(ids)):
            self.fields[str(produto.id)] = forms.DecimalField(label=str(produto), initial=produto.valor_unitario)

    def processar(self):
        for pk in self.cleaned_data:
            Produto.objects.filter(pk=pk).update(valor_unitario=self.cleaned_data[str(pk)])
        self.instance.recalcular()


class VincularProdutoAlmoxarifadoForm(forms.Form):
    produto = forms.ModelChoiceField(Produto.objects, label='Produto')
