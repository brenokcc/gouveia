# -*- coding: utf-8 -*-

import os
from gouveia.models import NotaFiscalManager
from django.conf import settings
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    def handle(self, *args, **options):
        dir_path = os.path.join(settings.MEDIA_ROOT, 'nfes')
        for file_name in os.listdir(dir_path):
            with open(os.path.join(dir_path, file_name), 'rb') as file:
                print(file.name)
                NotaFiscalManager.importar(file)
